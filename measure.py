import numpy as np
import cv2
import BinocularMeas as bl

folder = 'calipics/'
# 读取相机内外参的txt文件
file = open('parameters.txt', 'r')
P = (np.array([x.strip() for x in file.readlines()])).astype(np.float64)
# 图像的分辨率
imageSize=(1280,1024)
# 读取双目相机拍摄的棋盘格标定板图像
grayImageL = cv2.imread(folder+"L2.bmp", 0)
grayImageR = cv2.imread(folder+"R2.bmp", 0)
# 图像校正后图像计算棋盘格相邻角点间距和基准距离的绝对误差
error=bl.calCalibrationAccuracyFromCameraParameters(P,grayImageL,grayImageR)
print('绝对误差',error)

# 读取相机拍摄的光斑图像
imgL = cv2.imread(folder+'L1.bmp', 0)
imgR = cv2.imread(folder+'R1.bmp', 0)
# 光斑的三维坐标
w=np.array(bl.calCoordinateFromSpotcentroid(P,imgL,imgR))
print('光斑的三维坐标',w)



