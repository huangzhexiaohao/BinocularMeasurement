'''
利用双目视觉测量原理，计算图片中单目标的空间三维坐标，以LED光斑为例
'''
import numpy as np
import cv2
import BinocularMeas as bl

folder = 'calipics/'
# 读取相机内外参的txt文件
file = open('parameters.txt', 'r')
P = (np.array([x.strip() for x in file.readlines()])).astype(np.float64)

# 读取相机拍摄的光斑图像
imgL = cv2.imread(folder+'L1.bmp', 0)
imgR = cv2.imread(folder+'R1.bmp', 0)
# 光斑的三维坐标
w=np.array(bl.calCoordinateFromSpotcentroid(P,imgL,imgR))
print('光斑的三维坐标',w)